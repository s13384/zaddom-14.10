import java.util.ArrayList;
import java.util.Date;


public class Order implements IVisitable {

	private String number;
	private double orderTotalPrice;
	private Date orderDate;
	private ArrayList<Product> products;
	
	Order()
	{
		orderDate=new Date();
		orderTotalPrice=0;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		
		visitor.visit(this);

	}

	@Override
	public String giveReport() {
		
		return "Order total price is " + orderTotalPrice + " number of products " + products.size();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}


	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public void addProduct(Product product)
	{
		products.add(product);
		orderTotalPrice+=product.getPrice();
		
	}
	public ArrayList<Product> getProducts()
	{
		return products;
	}

}
