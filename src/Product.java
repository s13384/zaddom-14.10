
public class Product implements IVisitable {

	private String name;
	private double price;
	@Override
	public void accept(IVisitor visitor) {
		
		visitor.visit(this);

	}

	@Override
	public String giveReport() {
		
		return "My name is " + name + " my price is " + price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
