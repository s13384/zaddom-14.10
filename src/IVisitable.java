
public interface IVisitable {

	public void accept(IVisitor visitor);
	public String giveReport();
}
