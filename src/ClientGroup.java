import java.util.ArrayList;


public class ClientGroup implements IVisitable {

	private String memberName;
	private ArrayList<Client> clients;
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);

	}

	@Override
	public String giveReport() {
		
		return "My group name is " + memberName + " i have " + clients.size() + " clients";
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public void addClient(Client client)
	{
		clients.add(client);
	}
	public ArrayList<Client> getClients()
	{
		return clients;
	}

}
