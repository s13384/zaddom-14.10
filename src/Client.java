import java.util.ArrayList;


public class Client implements IVisitable {

	private String number;
	private ArrayList<Order> orders;
	@Override
	public void accept(IVisitor visitor) {
		
		visitor.visit(this);

	}

	@Override
	public String giveReport() {

		return "Number of clients " + orders.size();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public void addOrder(Order order)
	{
		orders.add(order);
	}
	public ArrayList<Order> getOrders()
	{
		return orders;
	}

}
