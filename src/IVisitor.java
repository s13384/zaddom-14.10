
public interface IVisitor {

	public void visit(IVisitable v);
}
