
public class ReportMaker implements IVisitor {

	private int numberOfClients;
	private int numberOfOrders;
	private int numberOfProducts;
	private String report;
	
	public int getNumberOfClients() {
		return numberOfClients;
	}
	public void setNumberOfClients(ClientGroup cG) {
		numberOfClients = cG.getClients().size();
	}
	public int getNumberOfOrders() {
		return numberOfOrders;
	}
	public void setNumberOfOrders(ClientGroup cG) {
		numberOfOrders=0;
		for (Client client : cG.getClients())
		{
			numberOfOrders+=client.getOrders().size();
		}
	}
	public int getNumberOfProducts() {
		return numberOfProducts;
	}
	public void setNumberOfProducts(ClientGroup cG) {
		numberOfProducts=0;
		for (Client client : cG.getClients())
		{
			for (Order order : client.getOrders())
			{
				numberOfProducts+=order.getProducts().size();
			}
		}
	}
	@Override
	public void visit(IVisitable v) {

		String tmpReport = v.giveReport();
		System.out.println(tmpReport);
		
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}

}
